from logging import DEBUG
from colorlog import getLogger, ColoredFormatter, StreamHandler


def _get_formatter():
    return ColoredFormatter(
        '%(log_color)s%(asctime)s [%(levelname)-8s] %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',  # Customize the timestamp format
        log_colors={
            'DEBUG': 'green',
            'INFO': 'white',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red,bg_white',
        },
    )


def get_logger():
    """
    Return a configured logger (logging) with colored syntax (colorlog)
    """
    # Get a new mylogger with colors support
    logging_logger = getLogger()
    # Set the minimum logging level
    logging_logger.setLevel(DEBUG)

    # Create a StreamHandler to output log messages to the console with colors
    console_handler = StreamHandler()
    if console_handler.formatter is None:
        console_handler.setFormatter(_get_formatter())
    logging_logger.addHandler(console_handler)

    return logging_logger
