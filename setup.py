from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="mylogger",
    version="1.0.0",
    author="laucoin",
    author_email="luc.aucoin1998@gmail.com",
    description="Configured logger, to avoid configure it each time.",
    url="https://gitlab.com/laucoin/my-logger",
    packages=find_packages(),
    install_requires=[
        'colorlog>=6.7.0',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires='>=3.11',
)

