# My Logger (Python library)

<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-1-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->

## This repository

This repository was created to avoid configuring python logger each time (it's a quick win).
The project target is to provide my custom logger configuration to my projects. It's not configurable and that's why the library is not published.

## How to install and use it ?

### Prerequisites

Install [Python3](https://www.python.org/downloads/)

> The project was built with python 3.11. Keep in mind that the library could not work properly with Python < 3.11.

### Build and run locally

1. Clone this repository with the following command:
    ```bash
    git clone git@gitlab.com:laucoin/my-logger.git ~/Documents/Git/Perso/my-logger
    ```
    OR
    ```bash
    git clone https://gitlab.com:laucoin/my-logger.git ~/Documents/Git/Perso/my-logger
    ```
2. Move to the right folder
    ```bash
    cd ~/Documents/Git/Perso/my-logger
    ```
3. Build the library
    ```bash
    python3 setup.py bdist_wheel
    ```
4. Install the library
    ```bash
    pip3 install dist/*.whl
    ```
5. Now, you can import and use the library in your projects
    ```python
    #!/usr/bin/env python3
    from mylogger import mylogger
   
    logger = mylogger.get_logger()

    logger.info('info')
    logger.debug('debug')
    logger.warning('warning')
    logger.error('error')
    ```
   Example of output:
   ![output.png](docs/output.png)

## Contributing

The `main` branch contains the stable code.

If you have more question, please have a look on [contributing file](https://gitlab.com/laucoin/global-readme/-/blob/main/CONTRIBUTING.md)

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tbody>
    <tr>
      <td align="center"><a href="https://luc-aucoin.fr"><img src="https://avatars.githubusercontent.com/u/31480129?v=4?s=100" width="100px;" alt="Luc AUCOIN"/><br /><sub><b>Luc AUCOIN</b></sub></a><br /><a href="https://github.com/laucoin/git-resources/commits?author=laucoin" title="Code">💻</a> <a href="https://github.com/laucoin/git-resources/commits?author=laucoin" title="Documentation">📖</a> <a href="#maintenance-laucoin" title="Maintenance">🚧</a></td>
    </tr>
  </tbody>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification.
Contributions of any kind welcome!
